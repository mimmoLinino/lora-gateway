/*
 / _____)             _              | |
( (____  _____ ____ _| |_ _____  ____| |__
 \____ \| ___ |    (_   _) ___ |/ ___)  _ \
 _____) ) ____| | | || |_| ____( (___| | | |
(______/|_____)_|_|_| \__)_____)\____)_| |_|
  (C)2013 Semtech-Cycleo

Description:
    Host specific functions to address the LoRa concentrator registers through
    a SPI interface.
    Single-byte read/write and burst read/write.
    Does not handle pagination.
    Could be used with multiple SPI ports in parallel (explicit file descriptor)

License: Revised BSD License, see LICENSE.TXT file include in the project
Maintainer: Sylvain Miermont
*/


/* -------------------------------------------------------------------------- */
/* --- DEPENDANCIES --------------------------------------------------------- */

#include <stdint.h>        /* C99 types */
#include <stdio.h>        /* printf fprintf */
#include <stdlib.h>        /* malloc free */
#include <unistd.h>        /* lseek, close */
#include <fcntl.h>        /* open */
#include <strings.h>       
#include <string.h>        /* memset */
#include <termios.h> 

#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <pthread.h>
#include <errno.h>

extern int errno;


#include "loragw_spi.h"
#include "loragw_hal.h"

/* -------------------------------------------------------------------------- */
/* --- PRIVATE MACROS ------------------------------------------------------- */

#define ARRAY_SIZE(a) (sizeof(a) / sizeof((a)[0]))
#define ERROR_MSG(str)                fprintf(stderr, str)
#define ERROR_PRINTF(fmt, args...)    fprintf(stderr,"%s:%d: "fmt, __FUNCTION__, __LINE__, args)

#if DEBUG_SPI == 1
    #define DEBUG_MSG(str)                fprintf(stderr, str)
    #define DEBUG_PRINTF(fmt, args...)    fprintf(stderr,"%s:%d: "fmt, __FUNCTION__, __LINE__, args)
    #define CHECK_NULL(a)                if(a==NULL){fprintf(stderr,"%s:%d: ERROR: NULL POINTER AS ARGUMENT\n", __FUNCTION__, __LINE__);return LGW_SPI_ERROR;}
    #define DUMP_BUFFER(a, b)			for (int i=0; i<b; ++i) { if (i==0) fprintf(stderr, "["); fprintf(stderr, "0x%x ", a[i]); if (i==b-1) fprintf(stderr, "]");}
    #define DUMP_BUFFER_R(a, b)			do { fprintf(stderr, "-R- "); DUMP_BUFFER(a, b);  fprintf(stderr, " ");} while(0);
    #define DUMP_BUFFER_W(a, b)			do { fprintf(stderr, "-W- "); DUMP_BUFFER(a, b);  fprintf(stderr, " ");} while(0);
    #define DUMP_STATS(a, b)			do { fprintf(stderr, "-S- "); fprintf(stderr, "nw=%d nr=%d ", a, b);} while(0);
#else
	#define DEBUG_TIMEOUT_MSG(str)		fprintf(stderr, str)
    #define DEBUG_MSG(str)
    #define DEBUG_PRINTF(fmt, args...)
    #define CHECK_NULL(a)                if(a==NULL){return LGW_SPI_ERROR;}
    #define DUMP_BUFFER(a, b)
    #define DUMP_BUFFER_R(a, b)
    #define DUMP_BUFFER_W(a, b)
    #define DUMP_STATS(a, b)		do { fprintf(stderr, "-S- "); fprintf(stderr, "nw=%d nr=%d ", a, b);} while(0);
#endif

#define BAUDRATE B115200
#define BAUDRATE_TO_RESET_SX1301	B600
#define BAUDRATE_FOR_CS_ENABLED		B1200
#define BAUDRATE_FOR_CS_DISABLED	B2400

#define SPI_SPEED       8000000
#define SPI_DEV_PATH    "/dev/ttyLRA"

#define READ_TIMEOUT 3
#define MAX_BUF_SIZE 128

typedef struct read_ctx_s {
  int fd;
  unsigned char buf[MAX_BUF_SIZE];
  int nr;
  int ntor;
  struct timeval timeout;
} read_ctx_t;

void *read_function(void *arg)
{
  read_ctx_t *ctx = (read_ctx_t *)arg;
  int num_bytes;
  int offset = 0;
  fd_set set;
  int rv;
  struct timeval timeout;
  int exit = 0;

  do
  {
    FD_ZERO(&set); /* clear the set */
    FD_SET(ctx->fd, &set); /* add our file descriptor to the set */

    timeout.tv_sec = ctx->timeout.tv_sec;
    timeout.tv_usec = ctx->timeout.tv_usec;

    rv = select(ctx->fd + 1, &set, NULL, NULL, &timeout);
    if(rv == -1)
    {
      exit = 1;
      ERROR_MSG("ERROR: Inside select\n"); /* an error accured */
    }
    else if(rv == 0)
    {
      exit = 1;
      DEBUG_MSG("select timeout\n"); /* a timeout occured */
    }
    else
    {
      offset = ctx->nr;

      num_bytes = read( ctx->fd, &ctx->buf[offset], (ctx->ntor - offset) ); /* there was data to read */

      if(num_bytes > 0) {
        ctx->nr += num_bytes;
      }else
      {
        ERROR_PRINTF("ctx->nr=%d, errno=%d\n", ctx->nr, errno);
      }
      num_bytes = 0;

      if(ctx->nr >= ctx->ntor)
      {
        exit = 1;
      }
    }
  }while(exit == 0);

  return NULL;
}


int setSerialSpeed(int _dev, speed_t _speed)
{
	struct termios options;
    tcgetattr(_dev, &options);
    cfsetispeed(&options, _speed);
    cfsetospeed(&options, _speed);
    // Set the new attributes
    int ret = tcsetattr(_dev, TCSANOW, &options);

    if (ret != 0)
    {
    	DEBUG_MSG("Error setting baudrate:\n");
    	perror("stdin");
    }
	return ret;
}


int enable_CS(int dev)
{
	return setSerialSpeed(dev, BAUDRATE_FOR_CS_ENABLED);
}

int disable_CS(int dev)
{
	return setSerialSpeed(dev, BAUDRATE_FOR_CS_DISABLED);
}

int reset_SX1301(int dev)
{
    return setSerialSpeed(dev, BAUDRATE_TO_RESET_SX1301);
}

int set_default_speed(int dev)
{
    return setSerialSpeed(dev, BAUDRATE);
}


/* -------------------------------------------------------------------------- */
/* --- PRIVATE CONSTANTS ---------------------------------------------------- */

#define READ_ACCESS     0x00
#define WRITE_ACCESS    0x80

uint8_t dummy[LGW_BURST_CHUNK] = {0};

/* -------------------------------------------------------------------------- */
/* --- PUBLIC FUNCTIONS DEFINITION ------------------------------------------ */

/* SPI initialization and configuration */
int lgw_spi_open(void **spi_target_ptr) {
    int *spi_device = NULL;
    int dev;
    struct termios options;
    read_ctx_t *r_ctx = NULL;

    /* check input variables */
    CHECK_NULL(spi_target_ptr); /* cannot be null, must point on a void pointer (*spi_target_ptr can be null) */

    /* allocate memory for the device descriptor */
    spi_device = malloc(sizeof(int));
    if (spi_device == NULL) {
        DEBUG_MSG("ERROR: MALLOC FAIL\n");
        return LGW_SPI_ERROR;
    }
    
     /* open SPI device */
	dev = open(SPI_DEV_PATH, O_RDWR | O_NOCTTY);
    if (dev < 0) {
        DEBUG_PRINTF("ERROR: failed to open SPI device %s\n", SPI_DEV_PATH);
        return LGW_SPI_ERROR;
    }

	DEBUG_PRINTF("Openened %s dev:%d\n", SPI_DEV_PATH, dev);

     
    fcntl(dev, F_SETFL);            // Configure port reading
    tcgetattr(dev, &options);       // Get the current options for the port
    cfsetispeed(&options, B115200);    // Set the baud rates to 230400
    cfsetospeed(&options, B115200);

    options.c_cflag |= (CLOCAL | CREAD);    // Enable the receiver and set local mode
    options.c_cflag &= ~PARENB;             // No parity bit
    options.c_cflag &= ~CSTOPB;             // 1 stop bit
    options.c_cflag &= ~CSIZE;              // Mask data size
    options.c_cflag |=  CS8;                // Select 8 data bits
    options.c_cflag &= ~CRTSCTS;            // Disable hardware flow control  
    options.c_cc[VMIN]   =  0;              // read blocks
    options.c_cc[VTIME]  =  0;              // 0.5 seconds read timeout

    // Enable data to be processed as raw input
    options.c_lflag &= ~(ICANON | ECHO | ISIG);

	/* Make raw */
	cfmakeraw(&options);
	
	tcflush( dev, TCIFLUSH );
	tcflush( dev, TCOFLUSH );
	
    // Set the new attributes
    tcsetattr(dev, TCSANOW, &options);

    *spi_device = dev;
    *spi_target_ptr = (void *)spi_device;
    DEBUG_MSG("Note: SPI port opened and configured ok\n");

    fcntl(dev, F_SETFL, fcntl(dev, F_GETFL) & ~O_NONBLOCK & ~O_ASYNC);

	/* Read spurious data if any */
	r_ctx = (read_ctx_t *)calloc(1, sizeof(read_ctx_t));

    if(r_ctx == NULL) {
        DEBUG_MSG("ERROR: failed to create r_ctx\n");
        return LGW_SPI_ERROR;
    }

    r_ctx->fd = dev;
    r_ctx->ntor = MAX_BUF_SIZE;
    r_ctx->timeout.tv_sec = READ_TIMEOUT;
    r_ctx->timeout.tv_usec = 0;

	read_function((void *)r_ctx);

    free(r_ctx);

    if (reset_SX1301(dev) == 0)
    {
        DEBUG_MSG("Note: radio reset ok\n");
    }
    set_default_speed(dev);

    sleep(3);

    return LGW_SPI_SUCCESS;
}

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

/* SPI release */
int lgw_spi_close(void *spi_target) {
    int spi_device;
    int a;

    /* check input variables */
    CHECK_NULL(spi_target);

    /* close file & deallocate file descriptor */
    spi_device = *(int *)spi_target; /* must check that spi_target is not null beforehand */
    a = close(spi_device);
    free(spi_target);

    /* determine return code */
    if (a < 0) {
        DEBUG_MSG("ERROR: SPI PORT FAILED TO CLOSE\n");
        return LGW_SPI_ERROR;
    } else {
        DEBUG_MSG("Note: SPI port closed\n");
        return LGW_SPI_SUCCESS;
    }
}


/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

/* Simple write */
int lgw_spi_w(void *spi_target, uint8_t spi_mux_mode, uint8_t spi_mux_target, uint8_t address, uint8_t data) {
    int spi_device;
    uint8_t out_buf[3];
    uint8_t command_size;
    int nw, nr;
    pthread_t read_thread;
    read_ctx_t *r_ctx = NULL;

    //DEBUG_MSG("\n**SIMPLE WRITE ** - ");
    /* check input variables */
    CHECK_NULL(spi_target);
    if ((address & 0x80) != 0) {
        //DEBUG_MSG("WARNING: SPI address > 127\n");
    }

    spi_device = *(int *)spi_target; /* must check that spi_target is not null beforehand */

    /* prepare frame to be sent */
    if (spi_mux_mode == LGW_SPI_MUX_MODE1) {
        out_buf[0] = spi_mux_target;
        out_buf[1] = WRITE_ACCESS | (address & 0x7F);
        out_buf[2] = data;
        command_size = 3;
    } else {
        out_buf[0] = WRITE_ACCESS | (address & 0x7F);
        out_buf[1] = data;
        command_size = 2;
    }

    r_ctx = (read_ctx_t *)calloc(1, sizeof(read_ctx_t));

    if(r_ctx == NULL) {
        DEBUG_MSG("ERROR: failed to create r_ctx\n");
        return LGW_SPI_ERROR;
    }

    r_ctx->fd = spi_device;
    r_ctx->ntor = command_size;
    r_ctx->timeout.tv_sec = READ_TIMEOUT;
    r_ctx->timeout.tv_usec = 0;

    /* create read thread */
    if(pthread_create(&read_thread, NULL, read_function, r_ctx) != 0) {
        DEBUG_MSG("ERROR: failed to create thread\n");
        free(r_ctx);
        return LGW_SPI_ERROR;
    }

    enable_CS(spi_device);
    nw = write(spi_device, &out_buf[0], command_size);

    pthread_join(read_thread, NULL);

    nr = r_ctx->nr;

    free(r_ctx);

    disable_CS(spi_device);
	
    /* determine return code */
    if ((nw != command_size)||(nr != command_size)) {
		DUMP_BUFFER_W( out_buf, command_size);
		DUMP_BUFFER_R( dummy, nr);
		DUMP_STATS(nw, nr);
        DEBUG_MSG("[FAIL]\n");
        return LGW_SPI_ERROR;
    } else {
        return LGW_SPI_SUCCESS;
    }
}

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

/* Simple read */
int lgw_spi_r(void *spi_target, uint8_t spi_mux_mode, uint8_t spi_mux_target, uint8_t address, uint8_t *data) {
    int spi_device;
    uint8_t out_buf[3];
    uint8_t command_size;
    uint8_t in_buf[3];
    int nw, nr;
    pthread_t read_thread;
    read_ctx_t *r_ctx = NULL;

    /* check input variables */
    CHECK_NULL(spi_target);
    if ((address & 0x80) != 0) {
        DEBUG_MSG("WARNING: SPI address > 127\n");
    }
    CHECK_NULL(data);

    spi_device = *(int *)spi_target; /* must check that spi_target is not null beforehand */

    /* prepare frame to be sent */
    if (spi_mux_mode == LGW_SPI_MUX_MODE1) {
        out_buf[0] = spi_mux_target;
        out_buf[1] = READ_ACCESS | (address & 0x7F);
        out_buf[2] = 0x00;
        command_size = 3;
    } else {
        out_buf[0] = READ_ACCESS | (address & 0x7F);
        out_buf[1] = 0x00;
        command_size = 2;
    }

    r_ctx = (read_ctx_t *)calloc(1, sizeof(read_ctx_t));

    if(r_ctx == NULL) {
        DEBUG_MSG("ERROR: failed to create r_ctx\n");
        return LGW_SPI_ERROR;
    }

    r_ctx->fd = spi_device;
    r_ctx->ntor = command_size;
    r_ctx->timeout.tv_sec = READ_TIMEOUT;
    r_ctx->timeout.tv_usec = 0;

    /* create read thread */
    if(pthread_create(&read_thread, NULL, read_function, r_ctx) != 0) {
        DEBUG_MSG("ERROR: failed to create thread\n");
        free(r_ctx);
        return LGW_SPI_ERROR;
    }
    
    enable_CS(spi_device);
    nw = write(spi_device, &out_buf[0], command_size);

    pthread_join(read_thread, NULL);

    nr = r_ctx->nr;
    memcpy(in_buf, r_ctx->buf, r_ctx->nr);

    free(r_ctx); 

    disable_CS(spi_device);

    /* determine return code */
    if ((nw != (int)command_size) || (nr != (int)command_size)) {
		DUMP_BUFFER_W( out_buf, command_size);
		DUMP_BUFFER_R( in_buf, command_size);
		DUMP_STATS(nw, nr);
		ERROR_PRINTF("[FAIL] write:%d read:%d\n", nw, nr);
        return LGW_SPI_ERROR;
    } else {
        *data = in_buf[command_size - 1];
        return LGW_SPI_SUCCESS;
    }
}

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

/* Burst (multiple-byte) write */
int lgw_spi_wb(void *spi_target, uint8_t spi_mux_mode, uint8_t spi_mux_target, uint8_t address, uint8_t *data, uint16_t size) {
    int spi_device;
    uint8_t command[2];
    uint8_t command_size;
    int size_to_do, chunk_size, offset;
    int byte_transfered = 0;
    int i, nw, nr;
    pthread_t read_thread;
    read_ctx_t *r_ctx = NULL;

	//DEBUG_MSG("\n**BURST MULTIPLE WRITE ** - ");
    /* check input parameters */
    CHECK_NULL(spi_target);
    if ((address & 0x80) != 0) {
        DEBUG_MSG("WARNING: SPI address > 127\n");
    }
    CHECK_NULL(data);
    if (size == 0) {
        DEBUG_MSG("ERROR: BURST OF NULL LENGTH\n");
        return LGW_SPI_ERROR;
    }

    spi_device = *(int *)spi_target; /* must check that spi_target is not null beforehand */

    /* prepare command byte */
    if (spi_mux_mode == LGW_SPI_MUX_MODE1) {
        command[0] = spi_mux_target;
        command[1] = WRITE_ACCESS | (address & 0x7F);
        command_size = 2;
    } else {
        command[0] = WRITE_ACCESS | (address & 0x7F);
        command_size = 1;
    }

    r_ctx = (read_ctx_t *)calloc(1, sizeof(read_ctx_t));

    if(r_ctx == NULL) {
        DEBUG_MSG("ERROR: failed to create r_ctx\n");
        return LGW_SPI_ERROR;
    }

    r_ctx->fd = spi_device;
    r_ctx->ntor = command_size;
    r_ctx->timeout.tv_sec = READ_TIMEOUT;
    r_ctx->timeout.tv_usec = 0;

    /* create read thread */
    if(pthread_create(&read_thread, NULL, read_function, r_ctx) != 0) {
        DEBUG_MSG("ERROR: failed to create thread\n");
        free(r_ctx);
        return LGW_SPI_ERROR;
    }

    enable_CS(spi_device);

    nw = write(spi_device, &command[0], command_size);

    pthread_join(read_thread, NULL);

    nr = r_ctx->nr;
    
    /* determine return code */
    if ((nw != command_size)||(nr != command_size))  {
        DUMP_STATS(nw, nr);
        ERROR_MSG("ERROR: SPI BURST WRITE FAILURE\n");
        return LGW_SPI_ERROR;
    }

    size_to_do = size;
    
    for (i=0; size_to_do > 0; ++i) {
        chunk_size = (size_to_do < LGW_BURST_CHUNK) ? size_to_do : LGW_BURST_CHUNK;
        offset = i * LGW_BURST_CHUNK;

        r_ctx->nr = 0;
        r_ctx->ntor = chunk_size;

        /* create read thread */
        if(pthread_create(&read_thread, NULL, read_function, r_ctx) != 0) {
             DEBUG_MSG("ERROR: failed to create thread\n");
             free(r_ctx);
             return LGW_SPI_ERROR;
        }

        nw = write(spi_device, &data[offset], chunk_size);
        byte_transfered += nw;

        pthread_join(read_thread, NULL);

        nr = r_ctx->nr;

	if (( nr != chunk_size)||(nw != chunk_size)) { ERROR_PRINTF("\n--WARNING -- %d %d --", i, offset); DUMP_STATS(nw, nr); }
        //DEBUG_PRINTF("BURST WRITE: to trans %d # chunk %d # transferred %d \n", size_to_do, chunk_size, byte_transfered);
        size_to_do -= chunk_size; /* subtract the quantity of data already transferred */
    }

    free(r_ctx);

    disable_CS(spi_device);

    /* determine return code */
    if (byte_transfered != size) {
	    DUMP_STATS(nw, nr);
        ERROR_MSG("[FAIL]\n");
        return LGW_SPI_ERROR;
    } else {
        return LGW_SPI_SUCCESS;
    }
}

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

/* Burst (multiple-byte) read */
int lgw_spi_rb(void *spi_target, uint8_t spi_mux_mode, uint8_t spi_mux_target, uint8_t address, uint8_t *data, uint16_t size) {
    int spi_device;
    uint8_t command[2];
    uint8_t command_size;

    int size_to_do, chunk_size, offset;
    int byte_transfered = 0;
    int i, nw, nr;
    pthread_t read_thread;
    read_ctx_t *r_ctx = NULL;

	//DEBUG_MSG("\n**BURST MULTIPLE READ ** - ");
    /* check input parameters */
    CHECK_NULL(spi_target);
    if ((address & 0x80) != 0) {
        DEBUG_MSG("WARNING: SPI address > 127\n");
    }
    CHECK_NULL(data);
    if (size == 0) {
        DEBUG_MSG("ERROR: BURST OF NULL LENGTH\n");
        return LGW_SPI_ERROR;
    }

    spi_device = *(int *)spi_target; /* must check that spi_target is not null beforehand */

    /* prepare command byte */
    if (spi_mux_mode == LGW_SPI_MUX_MODE1) {
        command[0] = spi_mux_target;
        command[1] = READ_ACCESS | (address & 0x7F);
        command_size = 2;
    } else {
        command[0] = READ_ACCESS | (address & 0x7F);
        command_size = 1;
    }

    r_ctx = (read_ctx_t *)calloc(1, sizeof(read_ctx_t));

    if(r_ctx == NULL) {
        DEBUG_MSG("ERROR: failed to create r_ctx\n");
        return LGW_SPI_ERROR;
    }

    r_ctx->fd = spi_device;
    r_ctx->ntor = command_size;
    r_ctx->timeout.tv_sec = READ_TIMEOUT;
    r_ctx->timeout.tv_usec = 0;

    /* create read thread */
    if(pthread_create(&read_thread, NULL, read_function, r_ctx) != 0) {
        DEBUG_MSG("ERROR: failed to create thread\n");
        free(r_ctx);
        return LGW_SPI_ERROR;
    }

    enable_CS(spi_device);
    nw = write(spi_device, &command[0], command_size);

    pthread_join(read_thread, NULL);

    nr = r_ctx->nr;

    /* determine return code */
     if ((nw != command_size)||(nr != command_size))  {
		        DUMP_STATS(nw, nr);
        ERROR_MSG("ERROR: SPI BURST WRITE FAILURE\n");
        return LGW_SPI_ERROR;
    }


    size_to_do = size;
    memset(&dummy, 0, sizeof(dummy)); /* clear dummy */
    memset(data, 0, size); /* clear data */
    for (i=0; size_to_do > 0; ++i) {
        chunk_size = (size_to_do < LGW_BURST_CHUNK) ? size_to_do : LGW_BURST_CHUNK;
        offset = i * LGW_BURST_CHUNK;

        r_ctx->nr = 0;
        r_ctx->ntor = chunk_size;

        /* create read thread */
        if(pthread_create(&read_thread, NULL, read_function, r_ctx) != 0) {
             DEBUG_MSG("ERROR: failed to create thread\n");
             free(r_ctx);
             return LGW_SPI_ERROR;
        }


        nw = write(spi_device, &dummy[0], chunk_size);

        pthread_join(read_thread, NULL);

        nr = r_ctx->nr;
        memcpy(&data[offset], r_ctx->buf, r_ctx->nr);
        byte_transfered += nr;
        
	if (( nr != chunk_size)||(nw != chunk_size)) { ERROR_PRINTF("\n--WARNING -- %d %d --", i, offset); DUMP_STATS(nw, nr); }
        //DEBUG_PRINTF("BURST READ: to trans %d # chunk %d # transferred %d \n", size_to_do, chunk_size, byte_transfered);
        size_to_do -= chunk_size; /* subtract the quantity of data already transferred */
    }

    free(r_ctx);

    disable_CS(spi_device);

    /* determine return code */
    if (byte_transfered != size) {

        ERROR_MSG("[FAIL]\n");
        return LGW_SPI_ERROR;
    } else {
        return LGW_SPI_SUCCESS;
    }
}

/* --- EOF ------------------------------------------------------------------ */
